/**
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

module "hierarchical_firewall_policy" {
  source = "../../modules/hierarchical_firewall_policy/"
  parent = data.google_active_folder.common.name
  name   = "com-firewall-rules"
  associations = [
    data.google_active_folder.common.name,
    # data.google_active_folder.bootstrap.name,
    data.google_active_folder.dev.name,
    data.google_active_folder.prod.name,
    data.google_active_folder.acc.name,
    data.google_active_folder.test.name,
    data.google_active_folder.sbx.name,
  ]
  rules = {
    delegate-rfc1918-ingress = {  # ranges to communicate with onprem - to add extra IP/rules
      description = "Delegate RFC1918 ingress"
      direction   = "INGRESS"
      action      = "goto_next"
      priority    = 500
      ranges = [
        "192.168.0.0/16",
        "10.240.0.0/16",
        "10.241.0.0/16",
        "10.242.0.0/16",
        "10.243.0.0/16",
        "10.244.0.0/16",
        "172.16.0.0/12"
      ]
      ports                   = { "all" = [] }
      target_service_accounts = null
      target_resources        = null
      logging                 = false
    }
    delegate-rfc1918-egress = {    # ranges to communicate with onprem - to add extra IP/rules
      description = "Delegate RFC1918 egress"
      direction   = "EGRESS"
      action      = "goto_next"
      priority    = 510
      ranges = [
        "192.168.0.0/16",
        "10.240.0.0/16",
        "10.241.0.0/16",
        "10.242.0.0/16",
        "10.243.0.0/16",
        "10.244.0.0/16",
        "172.16.0.0/12"
      ]
      ports                   = { "all" = [] }
      target_service_accounts = null
      target_resources        = null
      logging                 = false
    }
    allow-iap-ssh-rdp = {
      description = "Always allow SSH and RDP from IAP"
      direction   = "INGRESS"
      action      = "allow"
      priority    = 5000
      ranges      = ["35.235.240.0/20"]
      ports = {
        tcp = ["22", "3389"]
      }
      target_service_accounts = null
      target_resources        = null
      logging                 = var.firewall_policies_enable_logging
    }
    allow-windows-activation = {
      description = "Always outgoing Windows KMS traffic (required to validate Windows licenses)"
      direction   = "EGRESS"
      action      = "allow"
      priority    = 5100
      ranges      = ["35.190.247.13/32"]
      ports = {
        tcp = ["1688"]
      }
      target_service_accounts = null
      target_resources        = null
      logging                 = var.firewall_policies_enable_logging
    }
    allow-google-hbs-and-hcs = {
      description = "Always allow connections from Google load balancer and health check ranges"
      direction   = "INGRESS"
      action      = "allow"
      priority    = 5200
      ranges = [
        "35.191.0.0/16",
        "130.211.0.0/22",
        "209.85.152.0/22",
        "209.85.204.0/22"
      ]
      ports = {
        tcp = ["80", "443"]
      }
      target_service_accounts = null
      target_resources        = null
      logging                 = var.firewall_policies_enable_logging
    }
    docker-machines = {
    description = "Allow the Gitlab CI Agent to connect to the Gitlab CI Runners using SSH and Docker Machine"
    direction = "INGRESS"
    action = "allow"
    ranges = [
        "192.168.0.0/16",
        "10.240.0.0/16",
        "10.241.0.0/16",
        "10.242.0.0/16",
        "10.243.0.0/16",
        "10.244.0.0/16",
        "172.16.0.0/12"
    ]
    priority = 1000
    ports = {
      tcp = ["22","2376"]
    }
    target_service_accounts = null
    target_resources        = null
    logging                 = var.firewall_policies_enable_logging
    }
  }
}
