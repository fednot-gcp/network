/******************************************
  Provider credential configuration
 *****************************************/
provider "google" {
  credentials = var.GCP_CREDENTIALS
  # credentials = "../../gcp_creds.json"
}

provider "google-beta" {
  credentials = var.GCP_CREDENTIALS
  # credentials = "../../gcp_creds.json"
}

