/**
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

locals {
  parent_id        = var.parent_folder != "" ? "folders/${var.parent_folder}" : "organizations/${var.org_id}"
  env              = "common"
  environment_code = "com"
  mode             = var.enable_hub_and_spoke ? "spoke" : null
  bgp_asn_number   = "65001"
}

data "google_active_folder" "common" {
  display_name = local.env
  parent       = local.parent_id
}

data "google_active_folder" "dev" {
  display_name = "dev"
  parent       = local.parent_id
}

data "google_active_folder" "prod" {
  display_name = "prd"
  parent       = local.parent_id
}

data "google_active_folder" "test" {
  display_name = "tst"
  parent       = local.parent_id
}
data "google_active_folder" "acc" {
  display_name = "acc"
  parent       = local.parent_id
}
data "google_active_folder" "sbx" {
  display_name = "sbx"
  parent       = local.parent_id
}
