locals{
  mgmt_nic_condition = true
  mgmt_nic_ip_address_condition = local.mgmt_nic_condition ? "x-chkp-ip-address--public" : "x-chkp-ip-address--private"
  mgmt_nic_interface_condition = local.mgmt_nic_condition ? "x-chkp-management-interface--eth0" : "x-chkp-management-interface--eth1"
  network_defined_by_routes_condition = "x-chkp-topology-eth1--internal"
  network_defined_by_routes_settings_condition = "x-chkp-topology-settings-eth1--network-defined-by-routes"
  management_name               = "tf-checkpoint-management"                # "PLEASE ENTER MANAGEMENT NAME"
  configuration_template_name   = "tf-asg-autoprov-tmplt"    # "PLEASE ENTER CONFIGURATION TEMPLATE NAME"
}

resource "random_string" "random_sic_key" {
  length = 12
  special = false
}

resource "random_string" "random_string" {
  length = 5
  special = false
  upper = false
  keepers = {}
}

resource "google_service_account" "cloudguard" {
    project = local.base_net_hub_project_id
    account_id = "cloudguard"
    display_name = "cloudguard CME Management"
}

resource "google_compute_instance_template" "instance_template" {
  project = local.base_net_hub_project_id  
  #name = "cloudguard-tmplt-gcp-20220509"
  name_prefix = "cloudguard-tmplt-gcp-"
  machine_type = "n1-standard-4"
  can_ip_forward = true
  region = var.default_region1

  disk {
    source_image = "checkpoint-public/check-point-r8110-gw-byol-mig-335-883-v20210706"
    auto_delete = true
    boot = true
    device_name = "cloudguard-boot-${random_string.random_string.result}"
    disk_type = "pd-ssd"
    disk_size_gb = "100"
    mode = "READ_WRITE"
    type = "PERSISTENT"
  }

  network_interface {
    network = google_compute_network.vpc_com_shared_base_hub_ext.id
    subnetwork = google_compute_subnetwork.sb_com_shared_base_hub_ext_europe_west1.id
    subnetwork_project = local.base_net_hub_project_id
    dynamic "access_config" {
      for_each = local.mgmt_nic_condition ? [
        1] : []
      content {
        network_tier = local.mgmt_nic_condition ? "PREMIUM" : "STANDARD"
      }
    }
  }

  network_interface {
    network = module.base_shared_vpc[0].network_self_link
    subnetwork = module.base_shared_vpc[0].subnets_names[0]
    subnetwork_project = local.base_net_hub_project_id
  }

  lifecycle {
    create_before_destroy = true
  }

  scheduling {
    automatic_restart = true
    on_host_maintenance = "MIGRATE"
    preemptible = false
  }

  service_account {
    email = google_service_account.cloudguard.email
    scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append"]
  }
  tags = [
    format("x-chkp-management--%s", local.management_name),
    format("x-chkp-template--%s", local.configuration_template_name),
    "checkpoint-gateway",
    local.mgmt_nic_ip_address_condition,
    local.mgmt_nic_interface_condition,
    local.network_defined_by_routes_condition,
    local.network_defined_by_routes_settings_condition]

  metadata_startup_script = templatefile("./cloudguard/startup-script.sh", {
    // script's arguments
    generatePassword = "false"
    config_url = ""
    config_path = ""
    sicKey = "eAYeCmcrMZ9m8r5Y4iwrPwMEsPK0X5ZWDRO"
    allowUploadDownload = true
    templateName = "mgmt-tmpl-gcp-base-net-hub"
    templateVersion = "20201206"
    templateType = "terraform"
    mgmtNIC = "Ephemeral Public IP (eth0)"
    hasInternet = "false"
    enableMonitoring = true
    shell = "/etc/cli.sh"
    installationType = "AutoScale"
    computed_sic_key = random_string.random_sic_key.result
    managementGUIClientNetwork = ""
    primary_cluster_address_name = ""
    secondary_cluster_address_name = ""
    managementNetwork = "mgmt-gcp-base-net-hub"
  })

  metadata = {
    serial-port-enable = "true"
    instanceSSHKey = var.cloudguard_ssh_key
  }
}

resource "google_compute_firewall" "ICMP_firewall_rules" {
  project = local.base_net_hub_project_id  
  name = "cloudguard-fw-icmp-20220509"
  network = module.base_shared_vpc[0].network_self_link
  allow {
    protocol = "icmp"
  }
  source_ranges = ["217.111.180.226/32", "82.143.85.242/32", "213.246.197.130/32", "82.143.85.130/32"]
  target_tags = [
    "checkpoint-gateway"]
}
resource "google_compute_firewall" "TCP_firewall_rules" {
  project = local.base_net_hub_project_id  
  name = "cloudguard-fw-tcp-20220509"
  network = module.base_shared_vpc[0].network_self_link
  allow {
    protocol = "tcp"
  }
  source_ranges = ["217.111.180.226/32", "82.143.85.242/32", "213.246.197.130/32", "82.143.85.130/32"]
  target_tags = [
    "checkpoint-gateway"]
}

# temp rule to test from home ip
resource "google_compute_firewall" "ssh_from_home_ip" {
  project = local.base_net_hub_project_id  
  name = "ssh-from-home-ip"
  network = google_compute_network.vpc_com_shared_base_hub_ext.self_link
  allow {
    protocol = "tcp"
  }
  source_ranges = ["91.178.111.119/32"]
  target_tags = [
    "checkpoint-gateway"]
}

resource "google_compute_firewall" "TCP_healthcheck_rules" {
  project = local.base_net_hub_project_id  
  name = "cloud-guardgcp-health-check"
  network = module.base_shared_vpc[0].network_self_link
  allow {
    protocol = "tcp"
  }
  source_ranges = ["35.191.0.0/16", "130.211.0.0/22"]
  target_tags = [
    "checkpoint-gateway"]
}

resource "google_compute_firewall" "UDP_firewall_rules" {
  project = local.base_net_hub_project_id
  name = "cloudguard-fw-udp-20220509"
  network = module.base_shared_vpc[0].network_self_link
  allow {
    protocol = "udp"
  }
  source_ranges = ["217.111.180.226/32", "82.143.85.242/32", "213.246.197.130/32", "82.143.85.130/32"]
  target_tags = [
    "checkpoint-gateway"]
}
resource "google_compute_firewall" "SCTP_firewall_rules" {
  project = local.base_net_hub_project_id  
  name = "cloudguard-fw-sctp-20220509"
  network = module.base_shared_vpc[0].network_self_link
  allow {
    protocol = "sctp"
  }
  source_ranges = ["217.111.180.226/32", "82.143.85.242/32", "213.246.197.130/32", "82.143.85.130/32"]
  target_tags = [
    "checkpoint-gateway"]
}
resource "google_compute_firewall" "ESP_firewall_rules" {
  project = local.base_net_hub_project_id  
  name = "cloudguard-fw-esp-20220509"
  network = module.base_shared_vpc[0].network_self_link
  allow {
    protocol = "esp"
  }
  source_ranges = ["217.111.180.226/32", "82.143.85.242/32", "213.246.197.130/32", "82.143.85.130/32"]
  target_tags = [
    "checkpoint-gateway"]
}
resource "google_compute_region_instance_group_manager" "instance_group_manager" {
  project = local.base_net_hub_project_id  
  region = var.default_region1
  name = "cloudguard-igm-20220509"
  version {
    instance_template = google_compute_instance_template.instance_template.id
    name = "cloudguard-tmplt"
  }
  base_instance_name = "cloudguard-${random_string.random_string.result}"
}
resource "google_compute_region_autoscaler" "autoscaler" {
  project = local.base_net_hub_project_id  
  region = var.default_region1
  name = "cloudguard-autoscaler-20220509"
  target = google_compute_region_instance_group_manager.instance_group_manager.id

  autoscaling_policy {
    max_replicas = 8
    min_replicas = 2
    cooldown_period = 90

    cpu_utilization {
      target = 60/100
    }
  }
}

## Internal Load Balancer
resource "google_compute_forwarding_rule" "cloudguard_ilb_fr" {
project = local.base_net_hub_project_id
name = "cloudguard-euw1-fr"
region = var.default_region1
load_balancing_scheme = "INTERNAL"
backend_service = google_compute_region_backend_service.cloudguard_internal_backend_service.id
all_ports = true
allow_global_access = true
network = module.base_shared_vpc[0].network_self_link
subnetwork = module.base_shared_vpc[0].subnets_names[0]
ip_address = google_compute_address.cloudguard_euw1_iip.self_link
}
resource "google_compute_region_backend_service" "cloudguard_internal_backend_service" {
project = local.base_net_hub_project_id
name = "cloudguard-be"
region = var.default_region1
health_checks = [google_compute_health_check.cloudguard_be_internal_hs.id]
network = module.base_shared_vpc[0].network_self_link
backend {
group = google_compute_region_instance_group_manager.instance_group_manager.instance_group
}
}
resource "google_compute_health_check" "cloudguard_be_internal_hs" {
project = local.base_net_hub_project_id
name = "cloudguard-internal-hs"
check_interval_sec = 1
timeout_sec = 1
tcp_health_check {
port = "8117" # Set correct port...
}
}

resource "google_compute_address" "cloudguard_euw1_iip" {
project = local.base_net_hub_project_id
name = "cloudguard"
subnetwork = module.base_shared_vpc[0].subnets_names[0]
address_type = "INTERNAL"
address = "10.199.0.3"
region = var.default_region1
}



# resource "google_compute_address" "ext_lb_cloudguard_ipv4" {
#   name = "ext-lb-cloudguard-ipv4"
# }

# module "load_balancer" {
#   source       = "GoogleCloudPlatform/lb/google"
#   version      = "~> 2.0.0"
#   region       = var.default_region1
#   name         = "lb-tcp-cloudguard-ext"
#   ip_protocol  = "all"
#   service_port = 1-65535
#   network      = google_compute_network.vpc_com_shared_base_hub_ext.id
#   target_tags = ["checkpoint-gateway"]
# }

# module "lb-internal" {
#   source  = "GoogleCloudPlatform/lb-internal/google"
#   version = "4.5.0"
#   # insert the 13 required variables here
#   project = local.base_net_hub_project_id
#   network = module.base_shared_vpc.network_name
#   all_ports = true
#   region       = var.default_region1
#   name    = "ilb-tcp-cloudguard-int"
#   backends     = [
#     { group = google_compute_network.vpc_com_shared_base_hub_ext.id, description = "", failover = false }
#   ]
#   health_check = google_compute_region_health_check.ilb_cloudguard_healthcheck.name
#   source_ip_ranges = "0.0.0.0/0" # todo: limit range to GCP VPC peer networks
#   target_tags = ["checkpoint-gateway"]
#   source_tags = [""]
# }

# 




